var main = function() {

	$('form').submit(function(event) {
		var $input = $(event.target).find('input');
		var query = $input.val();

		if (query != "") {
			var html = $('<li>').text(query);
			html.appendTo('.search_results');
			$input.val("");
		}

		return false;
	});
}

$(document).ready(main);